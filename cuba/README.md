## Final Project

## Kelompok 4

## Anggota Kelompok


<ul>
    <li>Muhammad Ardhani</li>
    <li>Devi Yunidiansari</li>
</ul>

## Tema Project

<p>E-commerce Kopisop</p>

## ERD

<img src="public/gambar/erd2.png">

## Link Video

<p>Link Demo Aplikasi : https://drive.google.com/drive/folders/1TL2BOBZwwUgnOoMvQeQ1ilanQyabJGPF?usp=sharing </p>

<p>Link Deploy        : https://ahha.kopisop.sanbercodeapp.com/ </P>