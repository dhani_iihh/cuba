@extends('master')

@section('content')
<section class="vh-100 bg-image"
  style="background-image: url('https://mdbcdn.b-cdn.net/img/Photos/new-templates/search-box/img4.webp');">
    <div class="mask d-flex align-items-center h-100 gradient-custom-3">
    <div class="container h-100">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-12 col-md-9 col-lg-7 col-xl-6">
          <div class="card" style="border-radius: 15px;">
            <div class="card-body p-5">
              <h2 class="text-uppercase text-center mb-5">Buat Akun Baru</h2>

              <form>
                    @csrf
                    <div class="form-outline mb-4">
                        <label class="form-label" for="form3Example1cg">Nama</label><br>
                        <input type="text" class="form-control form-control-lg" />
                    </div><br>
                    @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-outline mb-4">
                        <label class="form-label" for="form3Example3cg">Email</label><br>
                        <input type="email" class="form-control form-control-lg" />
                    </div><br>
                    @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-outline mb-4">
                        <label class="form-label" for="form3Example4cg">Password</label><br>
                        <input type="password" class="form-control form-control-lg" />
                    </div><br>
                    @error('password')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="d-flex justify-content-center">
                        <button type="button" class="btn btn-success btn-block btn-lg gradient-custom-4 text-body"><a href="/profile/create">Register</a></button>
                    </div>
                    <p class="text-center text-muted mt-5 mb-0">Have already an account? <a href="#!"
                        class="fw-bold text-body"><u>Login here</u></a></p>
              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
@endsection
