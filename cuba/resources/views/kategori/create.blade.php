@extends('layouts.app')

@section('content')

<form action="/kategori" method="POST">
    @csrf
    <div class="form-group">
        <label>Jenis</label>
        <input name="jenis" type="string" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection