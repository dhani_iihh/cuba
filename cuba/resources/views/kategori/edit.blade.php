@extends('layouts.app')

@section('content')

<form action="/kategori/{{$kategori->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Jenis</label>
        <input name="jenis" value="{{$kategori->jenis}}" type="string" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection