@extends('layouts.app')

@section('content')

<a href="/kategori/create" class="btn btn-primary btn-sm my-3">Buat Jenis Barang</a>

<table class="table">
    <thead>
        <tr>
        <th scope="col">#</th>
        <th scope="col">Jenis Barang</th>
        <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
        @forelse($kategori as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->jenis}}</td>
                <td>
                    <form action="/kategori/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/kategori/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit"value="Delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>

        @empty
            <tr>
                <td>Belum ada jenis barang</td>
            </tr>

        @endforelse
</table>

@endsection