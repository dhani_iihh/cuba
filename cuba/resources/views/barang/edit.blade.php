@extends('layouts.app')

@section('content')

<form action="/barang/{{$barang->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Nama Barang</label>
        <input name="judul" value="{{$barang->judul}}" type="string" class="form-control">
    </div>
    <div class="form-group">
        <label>Gambar</label>
        <input name="gambar" type="file" class="form-control">
    </div>
    <div class="form-group">
        <label>Deskripsi Barang</label>
        <textarea name="deskripsi" cols="30" rows="10" class="form-control">{{$barang->deskripsi}}</textarea>
    </div>
    <div class="form-group">
        <label>Harga Barang</label>
        <input name="harga" type="integer" class="form-control">
    </div>
    <div class="form-group">
        <label>Kategori</label>
        <select class="form-control" name="kategori_id" id="">
            <option value="">--Pilih Kategori--</option>
            @forelse($kategori as $item)
                @if ($item->id === $barang->kategori_id)
                <option value="{{$item->id}}" selected>{{$item->jenis}}</option>

                @else
                <option value="{{$item->id}}" >{{$item->jenis}}</option>

                @endif
            @empty
                <option>Tidak ada Kategori</option>
            @endforelse
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection