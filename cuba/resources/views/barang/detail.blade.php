@extends('layouts.app')

@section('content')


<div class="row">
    <div class="col-12">
        <div class="card">
            <img src="{{asset('/gambarr/' . $barang->gambarr)}}" class="card-img-top" alt="...">
            <div class="card-body">
                <h2>{{$barang->judul}}</h2>
                <p class="card-text">{{$barang->deskripsi}}</p>
            </div>
        </div>
    </div>
</div>

<a href="/barang" class="btn btn-secondary btn-sm my-3">Kembali</a>


@endsection