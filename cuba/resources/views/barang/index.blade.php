@extends('layouts.app')

@section('content')

<a href="/barang/create" class="btn btn-primary btn-sm my-3">Buat Jenis Barang</a>

<div class="row">
    @forelse($barang as $item)
        <div class="col-4">
            <div class="card" style="width: 18rem;">
                <img src="{{asset('/gambarr/' . $item->gambarr)}}" class="card-img-top" height="400px" alt="...">
                <div class="card-body">
                    <h2>{{$item->judul}}</h2>
                    <p class="card-text">{{ Str::limit($item->deskripsi, 20) }}</p>
                    <a href="#" class="btn btn-primary">Shop More</a>
                    <a href="/barang/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
                </div>
            </div>
        </div>
    @empty
        Tidak ada Barang Pre-love
    @endforelse
</div>



@endsection