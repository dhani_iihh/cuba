@extends('layouts.app')

@section('content')

<form action="/barang" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Nama Barang</label>
        <input name="judul" type="string" class="form-control">
    </div>
    <div class="form-group">
        <label>Gambar</label>
        <input name="gambar" type="file" class="form-control">
    </div>
    <div class="form-group">
        <label>Deskripsi Barang</label>
        <textarea name="deskripsi" cols="30" rows="10" class="form-control"></textarea>
    </div>
    <div class="form-group">
        <label>Harga Barang</label>
        <input name="harga" type="integer" class="form-control">
    </div>
    <div class="form-group">
        <label>Kategori</label>
        <select class="form-control" name="kategori_id" id="">
            <option value="">--Pilih Kategori--</option>
            @forelse($kategori as $item)
                <option value="{{$item->id}}">{{$item->jenis}}</option>
            @empty
                <option>Tidak ada Kategori</option>
            @endforelse
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection