@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Buat Profil</div>

                <div class="card-body">
                    <form action="/profile" method="post">
                    @csrf
                        <div class="form-group">
                            <label for="inputAddress2">Alamat</label><br>
                            <input type="text" class="form-control" name="alamat">
                        </div> 
                        @error('alamat')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror<br><br>
                        <div class="form-group">
                            <label for="inputAddress">Umur</label><br>
                            <input type="integer" class="form-control" name="umur">
                        </div> 
                        @error('umur')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror<br><br>
                        <div class="form-group">
                            <label for="inputAddress2">Phone</label><br>
                            <input type="text" class="form-control" name="phone" placeholder="Format: 08xxxxxxxxxxx">
                        </div> 
                        @error('phone')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror<br><br>
                        <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection