@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Profil</div>

                <div class="card-body">
                    <h1>{{$profile->nama}}</h1>
                    <p>{{$profile->email}}</p>
                    <p>{{$profile->password}}</p>
                    <p>{{$profile->alamat}}</p>
                    <p>{{$profile->umur}}</p>
                    <p>{{$profile->phone}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
