@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                @forelse ($kategori as $key => $item)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$item->nama}}</td>
                        <td>
                            <form action="/profile/{{$item->id}}" method="post">
                                @csrf
                                @method('delete')
                                <a href="/profile/{{$item->id}}" class="btn btn-primary btn-sm my-3">Detail</a>
                                <a href="/profile/{{$item->id}}/edit" class="btn btn-warning btn-sm my-3">Edit</a>
                                <input type="submit" value="Delete" class="btn btn-danger btn-sm my-3">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>Belum ada data</td>
                    </tr>
                @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection