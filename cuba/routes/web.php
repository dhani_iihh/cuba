<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//crud user

Route::get('/', 'DashboardController@utama');
Route::get('/', 'DashboardController@belanja');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//create
Route::get('/', 'DashboardController@utama');
Route::get('/daftar', 'BiodataController@daftar');
Route::get('/profile/create', 'ProfileController@create');
Route::post('/profile', 'ProfileController@store');

//read 
Route::get('/profile', 'ProfileController@index');
Route::get('/profile/{profile_id}', 'ProfileController@show');

//update
Route::get('/profile/{profile_id}/edit', 'ProfileController@edit');
Route::put('/profile/{profile_id}', 'ProfileController@update');

//delete
Route::delete('/profile/{profile_id}', 'ProfileController@destroy');


//crud kategori

//create
Route::get('/kategori/create','KategoriController@create');
Route::post('/kategori', 'KategoriController@store');

//read
Route::get('/kategori', 'KategoriController@index');
Route::get('/kategori/{kategori_id}', 'KategoriController@show');

//update
Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit');
Route::put('/kategori/{kategori_id}', 'KategoriController@update');

//delete
Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy');

//crud barang
Route::resource('barang','BarangController');







