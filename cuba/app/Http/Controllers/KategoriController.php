<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KategoriController extends Controller
{
    public function create()
    {
        return view('kategori.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'jenis' => 'required',
        ]);

        DB::table('kategori')->insert([
            'jenis' => $request->jenis
        ]);

        return redirect('/kategori');
    }

    public function index()
    {
        $kategori = DB::table('kategori')->get();
        
        return view('kategori.index', ['kategori' => $kategori]);
    }

    public function show($id)
    {
        $kategori = DB::table('kategori')->find($id);

        return view('kategori.detail', ['kategori' => $kategori]);
    }

    public function edit($id)
    {
        $kategori = DB::table('kategori')->find($id);

        return view('kategori.detail', ['kategori' => $kategori]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'jenis' => 'required',
        ]);

        DB::table('kategori')
                ->where('id', $id)
                ->update(
                [
                    'jenis' => $request->jenis,
                ]);
        return redirect('/kategori');
    }

    public function destroy($id)
    {
        DB::table('kategori')->where('id', '=', $id)->delete();

        return redirect('/kategori');
    }
}
