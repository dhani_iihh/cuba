<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('profile.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'password' => 'required',
            'alamat' => 'required',
            'umur' => 'required|integer',
            'phone' => ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'min:10'],
            
        ],
        [
            'nama.required' => 'inputan nama harus diisi',
            'email.required' => 'inputan email harus diisi',
            'password.required' => 'inputan password harus diisi',
            'alamat.required' => 'inputan alamat harus diisi',
            'umur.integer' => 'inputan umur harus diisi dengan angka',
            'phone.regex' => 'inputan phone harus diisi dengan angka',
        ]);

        DB::table('profile')->insert(
            [
                'nama' => $request->nama,
                'email' => $request->email,
                'password' => $request->password,
                'alamat' => $request->alamat,
                'umur' => $request->umur,
                'phone' => $request->phone
            ]
            );

            return redirect('/profile');
    }

    public function index()
    {
        $profile = DB::table('profile')->get();
        
        return view('profile.index', ['profile' => $profile]);
    }

    public function show($id)
    {
        $profile = DB::table('profile')->find($id);

        return view('profile.detail', ['profile' => $profile]);
    }

    public function edit($id)
    {
        $profile = DB::table('profile')->find($id);

        return view('profile.edit', ['profile' => $profile]);
    }

    public function update($id, Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'password' => 'required',
            'alamat' => 'required',
            'umur' => 'required|integer',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:12',
            
        ],
        [
            'nama.required' => 'inputan nama harus diisi',
            'email.required' => 'inputan email harus diisi',
            'password.required' => 'inputan password harus diisi',
            'alamat.required' => 'inputan alamat harus diisi',
            'umur.integer' => 'inputan umur harus diisi dengan angka',
            'phone.regex' => 'inputan phone harus diisi dengan angka',
        ]);

        DB::table('profile')
            ->where('id', $id)
            ->update(
                [
                    'alamat' =>$request->alamat,
                    'umur' =>$request->umur,
                    'phone' =>$request->phone,
                ]
                );
        return redirect('/profile');
    }

    public function destroy($id)
    {
        DB::table('profile')->where('id', '=', $id)->delete();

        return redirect('/profile');
    }
}
