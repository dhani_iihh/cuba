<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except('belanja');
    }

    public function utama()
    {
        return view('page.index');
    }

    public function belanja()
    {
        return view('page.shop');
    }
}
