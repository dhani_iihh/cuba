<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use App\Barang;
use File;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = Barang::all();
        return view('barang.index', ['barang' => $barang]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        return view('barang.create', ['kategori' => $kategori]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'gambar' => 'required|image|',
            'deskripsi' => 'required',
            'harga' => 'required',
            'kategori_id' => 'required',
        ]);

        $namaGambar = time().'.'.$request->gambar->extension();

        $request->gambar->move(public_path('gambarr'), $namaGambar);

        $barang = new Barang;

        $barang->judul = $request->judul;
        $barang->deskripsi = $request->deskripsi;
        $barang->harga = $request->harga;
        $barang->kategori_id = $request->kategori_id;
        $barang->gambar = $namaGambar;

        $barang->save();

        return redirect('/barang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $barang = Barang::find($id);

        return view('barang.detail', ['barang' => $barang]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $barang = Barang::find($id);
        $kategori = Kategori::find($id);

        return view('barang.edit', ['barang' => $barang, 'kategori' => $kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'gambar' => 'image|max:2048',
            'deskripsi' => 'required',
            'harga' => 'required',
            'kategori_id' => 'required',
        ]);
        
        $barang = Barang::find($id);

        if ($request->has('gambar')) {
            $path = "gambarr/";
            File::delete($path . $barang->gambarr);
            $namaGambar = time().'.'.$request->gambar->extension();

             $request->gambar->move(public_path('gambarr'), $namaGambar);

             
        }else{

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
