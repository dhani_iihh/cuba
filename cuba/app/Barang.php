<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table="barang";

    protected $fillable=['judul', 'gambar', 'deskripsi', 'harga', 'kategori_id'];
}
